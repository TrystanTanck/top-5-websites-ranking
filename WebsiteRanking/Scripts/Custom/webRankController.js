//AngularJS module
var app = angular.module('app', []);

//AngularJS controller
app.controller('webRankController', ['$scope', '$http', webRankController]);

//AngularJS controller method

function webRankController($scope, $http) {
    $scope.selectedDate = new Date();

    $scope.loading = true; 
    $scope.addMode = false; 
 
    //Used to display the data 
    $http.get('http://localhost:15255/api/WebRanking/').success(function (data) {
        $scope.WebRankings = data;
        $scope.loading = false; 
    }) 
    .error(function () { 
        $scope.error = "An Error has occured while loading posts!"; 
        $scope.loading = false; 
    });

    //top 5 by date
    $scope.getTopFiveByDate = function () {

        $scope.stringDate = $scope.selectedDate.getFullYear() + "-" + ($scope.selectedDate.getMonth()+1) + "-" + $scope.selectedDate.getDate();
        $http.get('http://localhost:15255/api/WebRanking?selectedDate=' + $scope.stringDate).success(function (data) {
            $scope.WebRankings = data;
            $scope.loading = false;
        })
        .error(function () {
            $scope.error = "An Error has occured while loading posts!";
            $scope.loading = false;
        });
    }

    //top 5 by month
    $scope.getTopFiveByMonth = function () {

        $scope.month = $scope.selectedDate.getMonth() + 1;
        $scope.year = $scope.selectedDate.getFullYear();
        $http.get('http://localhost:15255/api/WebRanking?month=' + $scope.month + '&year=' + $scope.year).success(function (data) {
            $scope.WebRankings = data;
            $scope.loading = false;
        })
        .error(function () {
            $scope.error = "An Error has occured while loading posts!";
            $scope.loading = false;
        });
    }

    //top 5 by year
    $scope.getTopFiveByYear = function () {

        $scope.year = $scope.selectedDate.getFullYear();
        $http.get('http://localhost:15255/api/WebRanking?year=' + $scope.year).success(function (data) {
            $scope.WebRankings = data;
            $scope.loading = false;
        })
        .error(function () {
            $scope.error = "An Error has occured while loading posts!";
            $scope.loading = false;
        });
    }
  
} 