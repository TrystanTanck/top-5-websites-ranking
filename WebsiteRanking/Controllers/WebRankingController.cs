﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data.Entity; 
using WebsiteRanking.Models;
using System.Data.Entity.Infrastructure; 

namespace WebsiteRanking.Controllers
{
    public class WebRankingController : ApiController
    {
        private WebRankingModel db = new WebRankingModel();

        [HttpGet]
        public IEnumerable<WebRanking> Get()
        {
            return db.WebRankings.AsEnumerable();
        }

        [HttpGet]
        public IEnumerable<WebRanking> GetTopFiveByDate(string selectedDate)
        {
            var convertDate = Convert.ToDateTime(selectedDate);
            var web = db.WebRankings.Where(x => x.Date == convertDate).OrderByDescending(x => x.Visits).Take(5).AsEnumerable();
            if (web == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return web;
        }

        [HttpGet]
        public IEnumerable<WebRanking> GetTopFiveByMonth(int month, int year)
        {
            var web = db.WebRankings.Where(x => x.Date.Value.Month == month && x.Date.Value.Year == year).OrderByDescending(x => x.Visits).Take(5).AsEnumerable();
            if (web == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return web;
        }

        [HttpGet]
        public IEnumerable<WebRanking> GetTopFiveByYear(int year)
        {
            var web = db.WebRankings.Where(x => x.Date.Value.Year == year).OrderByDescending(x => x.Visits).Take(5).AsEnumerable();
            if (web == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return web;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        } 
    }
}
