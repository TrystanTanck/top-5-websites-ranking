namespace WebsiteRanking.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WebRanking")]
    public partial class WebRanking
    {
        public int Id { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(200)]
        public string Website { get; set; }

        public int? Visits { get; set; }
    }
}
