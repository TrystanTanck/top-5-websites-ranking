namespace WebsiteRanking.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class WebRankingModel : DbContext
    {
        public WebRankingModel()
            : base("name=WebRankingModel")
        {
        }

        public virtual DbSet<WebRanking> WebRankings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
